var _ = require('lodash');

let arr = [{
    "id": "1",
    "parent": "",
    "name": "foo",
  }, {
    "id": "2",
    "parent": "1",
    "name": "bar",
  }, {
    "id": "3",
    "parent": "",
    "name": "bazz",  
  }, {
    "id": "4",
    "parent": "3",
    "name": "sna",
  }, {
    "id": "5",
    "parent": "3",
    "name": "ney",
  }, {
    "id": "6",
    "parent": "5",
    "name": "tol",
  }, {
    "id": "7",
    "parent": "5",
    "name": "zap",
  }, {
    "id": "8",
    "parent": "",
    "name": "quz",
  }, {
    "id": "9",
    "parent": "8",
    "name": "meh",
  }, {
    "id": "10",
    "parent": "8",
    "name": "ror",
  }, {
    "id": "11",
    "parent": "",
    "name": "gig",
  }, {
    "id": "12",
    "name": "xylo",
    "parent": "",
  }, {
    "id": "13",
    "parent": "",
    "name": "grr",
  }];

  function makeCatTree(arr){
    //Добавляем поле children
  _.forEach(arr, function(item){
    item.children = [];
  });
 
  //Группируем элементы массива по полю родителей
  let groupedByParents = _.groupBy(arr, 'parent');
  //console.log(groupedByParents);

  /* Создаем дополнительный объект, в котором ключами свойств выступают значения их полей id*/
  let elementsByIndex = _.keyBy(arr, 'id');
  console.log(elementsByIndex);


  //В отдельную переменную записываем группы элементов у которых есть родители
  let elWithParents = _.omit(groupedByParents, '');
  console.log(elWithParents);

  //В поля children именованных по индексам объектов записываем соответствующий элемент из массива c родителями
  _.forEach(elWithParents,function(children, parentId){
    elementsByIndex[parentId].children = children;
  });
  //console.log(elementsByIndex);

  return groupedByParents[''];
  
  }
  
  makeCatTree(arr);