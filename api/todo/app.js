const port = 3000;
const host = '127.0.0.1';
const http = require('http');
const DB = require('./config/keys');
/* Подключаем фреймворк */
const express = require('express');
const app = express();




/* Подключаем пакет распознавания закодированных символов и применяем его ко всему приложению*/
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

//Configure our app
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/* Подключение к базе данных mongoDB*/
const mongoose = require('mongoose');
mongoose.connect(DB.mongoAdress);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(){
    console.log('Мы подключены!');
});

require('./components/Tasks/Model');
require('./components/Users/Model');
app.use(require('./components/Tasks/routes'));
app.use(require('./components/Users/routes'));
app.use(require('./components/Files/routes'));


const pid = process.pid;
app.listen(port, () => console.log(`Отслеживаем порт 3000. PID: ${pid}`));
