const mongoose = require('mongoose');
const Task =  mongoose.model('Task');

module.exports.showTasks = function(request, response, next){
    Task.find(function (err, task){
        if (err) return console.error(err);
        let show = [];
        task.forEach(function(singleTask){
            show.push(`${singleTask.id} : ${singleTask.showTask()}`);
        });
        response.send(show);
    });
    result = "";
};

module.exports.addNewTask =  function(request, response, next){
    let newTask = new Task({id: request.body.id, 
                           text: request.body.text,
                           date: request.body.date});
  
    newTask.save(function (err, createdTask){
        if (err) return console.error(err);
        response.send(createdTask);
    });
};

module.exports.getSingleTask = function(request, response, next){
    let taskId = request.query.id;
    console.log(request.query);
    Task.find({id: taskId}, function(err, task){
        if (err) return console.error(err);
        response.send(task[0].showTask());
    });
};

module.exports.deleteTask = function(request, response, next){
    let taskId = request.query.id;
    console.log('taskId');
    Task.deleteOne({id: taskId}, function(err){
        if (err) return console.error(err);
        response.send(`Task with id ${taskId} has been deleted!`);
    });
};

module.exports.updateTask = function(request, response, next){
    let id = request.query.id;
    let newText = request.body.text;
    let newTask = Task.update({id: id}, {text: newText}, function(err, row){
        if (err) return console.error(err);
        Task.find({id: id},function(err, updTask){
            response.send(updTask[0].showTask());
        });
    });
};

module.exports.selectMultipleTasks = function(req, res, next){
    let QueryLimits = {gt: 0, lt: new Date()};
    let QueryOptions = {};
    if(req.query.from){
        //fromDate = new Date(req.query.from);
        let fromDateArr = req.query.from.split('.');
        QueryLimits.gt = new Date(fromDateArr[2], fromDateArr[1], fromDateArr[0]);
    };

    if(req.query.to){
        //toDate = new Date(req.query.to);
        let toDateArr = req.query.to.split('.');
        QueryLimits.lt = new Date(toDateArr[2], toDateArr[1], toDateArr[0]);
    };

    if(+req.query.page > 0){
        let limit = 3;
        let minusTasks = req.query.page * limit - limit;
        QueryOptions.skip = minusTasks;
        QueryOptions.limit = limit;
    }

    let query = Task.find().lt('date', QueryLimits.lt).gt('date', QueryLimits.gt).setOptions(QueryOptions);

    query.exec(function (err, data) {
        if (err) return console.error(err);
        let taskMessage = [];
        data.forEach(function(singleTask){
            if(singleTask){
                taskMessage.push(singleTask.showTask());
            }
        });
        res.send(taskMessage); 
    });
};