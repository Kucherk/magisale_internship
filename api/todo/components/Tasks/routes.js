const mongoose = require('mongoose');
const router = require('express').Router();

/* Подключаем экшены для роутов */
const TasksActions = require('./actions');

/* Подключаем пакет распознавания закодированных символов и применяем его ко всему приложению*/
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));

/* Выводим список всех тасков */
router.get('/list', TasksActions.showTasks);
/* Добавлене нового таска */
router.post('/addTask', TasksActions.addNewTask);
//Получить конкретный таск по id
router.get('/getTask/', TasksActions.getSingleTask);
//Удаление таска по id
router.get('/delTask/', TasksActions.deleteTask);
//Изменение таска 
router.post('/updTask/', TasksActions.updateTask);
//Выборка тасков по дате
router.get('/getTasks/', TasksActions.selectMultipleTasks);
module.exports = router;