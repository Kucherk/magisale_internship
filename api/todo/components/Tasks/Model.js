const mongoose = require('mongoose');
/* Записываем новую схему будущей таблицы данных */
let tasksShema = mongoose.Schema({
    id: Number,
    text: String,
    date: Date
});

/* Создание метода  */
tasksShema.methods.showTask = function () {
    let greeting =`Номер таска: '${this.id}' Задание, которое тебе необходимо выполнить: '${this.text}'. Дата постановки задачи: ${this.date.getDate()}.${this.date.getMonth()}.${this.date.getFullYear()}`;
    return(greeting);
  };

/* Создаем модель на основе таблицы (класс)*/
mongoose.model('Task', tasksShema);