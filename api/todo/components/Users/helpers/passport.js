const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const GoogleStrategy = require('passport-google-oauth20');
const keys = require('../../../config/keys');

const User = mongoose.model('Users');

passport.use(new LocalStrategy({
  usernameField: 'user[email]',
  passwordField: 'user[password]',
}, (email, password, done) => {
  Users.findOne({ email })
    .then((user) => {
      if(!user || !user.validatePassword(password)) {
        return done(null, false, { errors: { 'email or password': 'is invalid' } });
      }

      return done(null, user);
    }).catch(done);
}));

passport.use(
  new GoogleStrategy({
    callbackURL: '/google/redirect',
    clientID: keys.Google.clientID,
    clientSecret: keys.Google.clientSecret
  },
  (accessTocen, refreshToken, profile, done) =>{
    console.log('passport callback function fired!');
    console.log(profile);
      new User({username: profile.displayName, 
                googleId: profile.id}).save()
                .then((newUser) => {
                  console.log(`New user created: 
                              ${newUser}`);
                });
  })
);