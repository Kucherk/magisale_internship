const mongoose = require('mongoose');
const Users = mongoose.model('Users');


module.exports.checkUserData = function(userData, responce){
    if(!userData.email) {
        return responce.status(422).json({
          errors: {
            email: 'is required',
          },
        });
    };
    
    if(!userData.password) {
        return responce.status(422).json({
          errors: {
            password: 'is required',
          },
        });
    };
};

module.exports.saveUser = function(user, responce){
    const finalUser = new Users(user);
  
    finalUser.setPassword(user.password);
  
    return finalUser.save()
      .then(() => responce.json({ user: finalUser.toAuthJSON() }));
};