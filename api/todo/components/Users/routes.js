const router = require('express').Router();
const auth = require('./helpers/auth');
require('./helpers/passport');
const passport = require('passport');
/* Подключаем экшены для роутов */
const UsersActions = require('./actions');

/* Подключаем пакет распознавания закодированных символов и применяем его ко всему приложению*/
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
//POST new user route (optional, everyone has access)
router.post('/', auth.optional, UsersActions.addNewUser);
//POST login route (optional, everyone has access)
router.post('/login', auth.optional, UsersActions.tryToLogin);
//GET current route (required, only authenticated users have access)
router.get('/current', auth.required, UsersActions.getCurrentUser);

//Google routers
router.get('/google', UsersActions.loginWithGoogle);
router.get('/google/redirect', passport.authenticate('google'), UsersActions.redirectUser);
router.get('/logout', UsersActions.logout);

module.exports = router;