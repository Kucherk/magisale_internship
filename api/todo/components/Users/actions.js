const mongoose = require('mongoose');
const Users = mongoose.model('Users');
const passport = require('passport');
const helpFunctions = require('./helpers/helpFunctions');

module.exports.addNewUser = (req, res, next) => {
    const { body: { user } } = req;
    let error = null;
    if( error = helpFunctions.checkUserData(user, res)){
      return error;
    }
    return helpFunctions.saveUser(user, res);
};

  module.exports.tryToLogin = (req, res, next) => {
    const { body: { user } } = req;
  
    let error = null;
    if( error = helpFunctions.checkUserData(user, res)){
      return error;
    }
  
    return passport.authenticate('local', { session: false }, (err, passportUser, info) => {
      if(err) {
        return next(err);
      }
  
      if(passportUser) {
        const user = passportUser;
        user.token = passportUser.generateJWT();
  
        return res.json({ user: user.toAuthJSON() });
      }
  
      return status(400).info;
    })(req, res, next);
  };

  module.exports.getCurrentUser = (req, res, next) => {
    const { payload: { id } } = req;
  
    return Users.findById(id)
      .then((user) => {
        if(!user) {
          return res.sendStatus(400);
        }
  
        return res.json({ user: user.toAuthJSON() });
      });
  };

  //Google login with passport
  module.exports.loginWithGoogle = passport.authenticate('google',{
    scope:['profile']
  });
  module.exports.redirectUser = (req, res) =>{
    res.send("You've reached the callback function! :)" );
  }

  module.exports.logout = (req, res, next) => {
    res.send('logout with Google');
  }
