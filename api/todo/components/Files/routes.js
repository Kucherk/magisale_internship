const mongoose = require('mongoose');
const router = require('express').Router();
const bodyParser = require('body-parser');

const FileActions = require('./actions');
const upload = require('express-fileupload');

router.use(upload());
router.use(bodyParser.urlencoded({ extended: true }));
router.post('/file', FileActions.uplFile);
module.exports = router;


