const cluster = require('cluster');
const os = require('os');
if(cluster.isMaster){
    let cpusCount = os.cpus().length;
    console.log(`Количество ядер: ${cpusCount}`);
    console.log(`Master started`);
    for(let i = 0; i < cpusCount-1; i++){
        let worker = cluster.fork();
        worker.on('exit', () => {
            cluster.fork();
        });
        
    }
     
}
if(cluster.isWorker){
    require('./app.js');

}